#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk AS build
WORKDIR /src
COPY ["NameService.csproj", "."]
RUN dotnet restore "./NameService.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "NameService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "NameService.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "NameService.dll"]