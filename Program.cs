using System.ComponentModel.DataAnnotations;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.EnableAnnotations();
    c.SchemaGeneratorOptions.SupportNonNullableReferenceTypes = true;
});
builder.Services.AddCors();
builder.Services.AddDbContextPool<NameContext>(opts => opts.UseSqlite("Data Source=entries.db;"));

var app = builder.Build();
await using (var scope = app.Services.CreateAsyncScope())
await using (var context = scope.ServiceProvider.GetRequiredService<NameContext>())
{
    context.Database.EnsureCreated();
}

app.UseSwagger();
app.UseSwaggerUI();
app.UseCors();

app.MapPut(
        "/entry",
        (HttpContext context, IServiceProvider sp, EntryInput input) =>
        {
            lock (app)
            {
                if (input.Name.Length > 512)
                    throw new BadHttpRequestException("Keep name below 512 characters please.");

                if (string.IsNullOrWhiteSpace(input.Name))
                    throw new BadHttpRequestException("Empty names not supported");

                using var db = sp.GetRequiredService<NameContext>();
                var address = !string.IsNullOrWhiteSpace(input.Address) ? input.Address
                    : context.Connection.RemoteIpAddress != null ? context.Connection.RemoteIpAddress.ToString()
                    : throw new BadHttpRequestException(
                        "No Address and couldn't figure it out IP address from connection");
                var entity = db.Entries.FirstOrDefault(e => e.Name == input.Name);
                if (entity == null)
                {
                    entity = new Entry { Name = input.Name };
                    db.Entries.Add(entity);
                }

                entity.Address = address;
                db.SaveChanges();
                return entity;
            }
        })
    .WithMetadata(new SwaggerOperationAttribute
    {
        Summary = "Add or update an entry",
        Description =
            "Insert or updates an entry in the database. If the address is not specified, the IP address of the incoming request is used."
    });

app.MapGet("/entry/{name}", (IServiceProvider sp, string name) =>
{
    lock (app)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new BadHttpRequestException("Empty names not supported");

        using var db = sp.GetRequiredService<NameContext>();
        return db.Entries.FirstOrDefault(e => e.Name == name);
    }
})
    .WithMetadata(new SwaggerOperationAttribute
    {
        Summary = "Get an entry by name",
        Description = "Gets an entry by name. If the entry does not exist, it will return null."
    });

app.MapGet("/entry", async (IServiceProvider sp) =>
    {
        await using var db = sp.GetRequiredService<NameContext>();
        return await db.Entries.ToListAsync();
    })
    .WithMetadata(new SwaggerOperationAttribute
    {
        Summary = "Get all entries",
        Description = "Gets all entries in the database."
    });


app.MapDelete("/entry/{name}/{address}", (IServiceProvider sp, string name, string address) =>
    {
        lock (app)
        {
            address = WebUtility.UrlDecode(address);
            using var db = sp.GetRequiredService<NameContext>();
            var entry = db.Entries.FirstOrDefault(e => e.Name == name && e.Address == address);
            if (entry is null)
                return Results.NoContent();
            db.Entries.Remove(entry);
            db.SaveChanges();
            return Results.Ok();
        }
    })
    .WithMetadata(new SwaggerOperationAttribute
    {
        Summary = "Delete an entry",
        Description =
            "Deletes an entry by name and address. If the entry does not exist, it will return 200 anyway (assume the entry is deleted after this call)."
    });

app.Run();

public record EntryInput(string Name, [MaxLength(512)] string? Address = null);

public class Entry
{
    [Key]
    public string Name { get; set; } = null!;

    [MaxLength(512)] public string Address { get; set; } = null!;

    public DateTime Updated { get; set; } = DateTime.Now;
}

public class NameContext : DbContext
{
    public NameContext(DbContextOptions<NameContext> opts) : base(opts)
    {

    }

    public DbSet<Entry> Entries { get; set; } = null!;
}